import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import { useState } from "react";
import { FruitsI } from "@/entities";
import Fruits from "../components/Fruits";
import { FormFruit } from "@/components/FormFruit";


export default function Index() {

  const [fruit, setFruit]= useState<FruitsI[]>([
    { id: 1, name: 'Banane', img: 'https://images.unsplash.com/photo-1481349518771-20055b2a7b24?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2139&q=80', texte: " ce fruit est delicieux" },
    { id: 2, name: 'Citron', img: 'https://images.unsplash.com/photo-1481349518771-20055b2a7b24?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2139&q=80', texte: " ce fruit est delicieux" },
    { id: 3, name: 'Poire', img: 'https://images.unsplash.com/photo-1481349518771-20055b2a7b24?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2139&q=80', texte: " ce fruit est delicieux" },
    { id: 4, name: 'Cerise', img: 'https://images.unsplash.com/photo-1481349518771-20055b2a7b24?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2139&q=80', texte: " ce fruit est delicieux" }

])

  return (
    <div className="container">
      <h2 className="h2 text-center m-4">Les fruits</h2>
      <h4>Ajouter</h4>
      <div className="my-5">
      <FormFruit/>
      </div>
     <div >
     <div className="row">
     {fruit.map((item) =>
        <Fruits key={item.id} name={item.name} img={item.img} texte={item.texte}/>
      )}
      </div>
    </div>
    </div>     
   


  );

}

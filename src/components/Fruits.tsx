import { FruitsI } from "@/entities"; 


const Fruits = (fruit:FruitsI)=>{
    return (
        <div className="col-3 ">
          <div className="card">
            <img src={fruit.img} alt=""  />
              <div className="card-body">
                <h5 className="card-title">{fruit.name}</h5>
                <p className="card-text">{fruit.texte} </p>
                <a href="#" className="btn btn-primary">Lire</a>
              </div>
          </div>
        </div>
    )
}

export default Fruits;